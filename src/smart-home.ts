import {Bulb} from './bulb'
import {LightSwitch} from './light-switch'

export class SmartHome {
    private bulb: Bulb
    private lightSwitch: LightSwitch

    constructor(bulb: Bulb, lightSwitch: LightSwitch) {
        this.bulb = bulb
        this.lightSwitch = lightSwitch
    }

    run() {
        if (this.lightSwitch.isOn()){
            this.bulb.turnOn()
            return
        }
        this.bulb.turnOff()
    }
}