export interface LightSwitch {
    returnValue: boolean
    isOn(): boolean
}