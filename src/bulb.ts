export interface Bulb {
    turnOnWasCalled: boolean
    turnOffWasCalled: boolean
    turnOn(): void
    turnOff(): void
}