import {SmartHome} from '../src/smart-home'
import {Bulb} from '../src/bulb'
import {LightSwitch} from '../src/light-switch'
import {BulbSpy} from './bulb-spy'
import {LightSwitchStub} from './light-switch-stub'

describe('smartHome', () => {
    let smartHome: SmartHome
    let bulbSpy: Bulb
    let switchStub: LightSwitch

    beforeEach(function () {
        bulbSpy = new BulbSpy()
        switchStub = new LightSwitchStub()
        smartHome = new SmartHome(bulbSpy, switchStub)
    })

    describe('スイッチがONの時', () => {
        it('電球が光る', () => {
            switchStub.returnValue = true

            smartHome.run()

            expect(bulbSpy.turnOnWasCalled).toBeTruthy()
            expect(bulbSpy.turnOffWasCalled).toBeFalsy()
        })
    })

    describe('スイッチがOFFの時', () => {
        it('電球が光らない', () => {
            switchStub.returnValue = false

            smartHome.run()

            expect(bulbSpy.turnOffWasCalled).toBeTruthy()
            expect(bulbSpy.turnOnWasCalled).toBeFalsy()
        })

    })
})