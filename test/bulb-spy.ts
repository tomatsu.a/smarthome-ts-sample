import {Bulb} from '../src/bulb'

export class BulbSpy implements Bulb {
    turnOffWasCalled = false
    turnOnWasCalled = false
    turnOff(): void {
        this.turnOffWasCalled = true
    }

    turnOn(): void {
        this.turnOnWasCalled = true
    }

}