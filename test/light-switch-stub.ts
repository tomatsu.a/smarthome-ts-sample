import {LightSwitch} from '../src/light-switch'

export class LightSwitchStub implements LightSwitch {
    returnValue = false

    isOn(): boolean {
        return this.returnValue
    }

}